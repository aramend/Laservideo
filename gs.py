import numpy as np
import sys
from math import ceil

p_data = open("d633.csv", "r").readlines()
p_data = [float(val) for val in p_data]
p_x = np.arange(1, len(p_data)+1)

polyarr = np.load("polyarr_16.npy")
n = int(np.sqrt(len(polyarr)))

def val_to_rad(target):
	#Sisendhologrammidel lineaarne faasimodulatsioon, kus 2pi'le vastab 193
	radfunc = lambda val: (2*np.pi/193)*val
	vfunc = np.vectorize(radfunc)
	return vfunc(target)

def rad_to_func_val(rad, poly):
	return int(np.round((poly - rad).roots[0]))

def csv_calib(target):

	# Sisend pval, väljund rad
	csv_func = np.poly1d(np.polyfit(p_data, p_x, 7))

	target_rad = val_to_rad(target)

	vfunc = np.vectorize(csv_func)

	return vfunc(target_rad)

def i_calib(target):

	x_len = 768 // n 
	y_len = 1024 // n
	
	target_rad = val_to_rad(target)

	for row_ind in range(n):
		for col_ind in range(n):

			x_start = row_ind * x_len
			x_end   = (row_ind + 1) * x_len
			y_start = col_ind * y_len
			y_end   = (col_ind + 1) * y_len

			polyfit_counter = (row_ind + 1) * (col_ind + 1) - 1
			vfunc = np.vectorize(np.poly1d(polyarr[polyfit_counter]))

			subarea = target_rad[x_start : x_end, y_start : y_end]
			target_rad[x_start : x_end, y_start : y_end] = vfunc(subarea)
		
	return target_rad.astype(np.uint8)




def fourier(a):
	 return np.fft.fft2(a)

def sfourier(a):
	return np.fft.fftshift(a)

def ifourier(a):
	return np.fft.ifft2(a)

def GerchbergSaxton(target, iterations):
	c = 0
	A = ifourier(target*np.exp(1j*np.random.rand(target.size).reshape(target.shape)*2*np.pi)) #A, B on hologrammid, C ja D difraktsioonipildid

	while c < iterations:
		B = np.exp(1j*np.angle(A))
		C = fourier(B)
		D = np.absolute(target) * np.exp(1j*np.angle(C))
		A = ifourier(D) 
		c += 1

	fin = ifourier(sfourier(np.fliplr(D)))
	
	return ((np.angle(fin)+np.pi)/(2*np.pi))*193



def progress(pikkus,järg):

	ribapikkus = 50
	suhe = järg/pikkus
	protsent = str(round(suhe*100,2))+"%"
	täidetud = ceil((järg/pikkus)*ribapikkus)

	sys.stdout.write("\r[%s%s] %s Valmis" % ("#"*täidetud,"-"*(ribapikkus-täidetud),protsent))
	sys.stdout.flush()



def aeg(sec):
	if sec > 0:

		if sec > 60:
			minutid = sec // 60
			sekundid = round((sec - minutid*60), 2)

		else:
			return str(round(sec,2))+" sekundit kulus."

		if minutid >= 60:
			tunnid = minutid // 60
			minutid = minutid - tunnid*60

			if minutid == 60:
				tunnid += 1

			return str(tunnid)+" tundi, "+str(int(minutid))+" minutit ja "+str(sekundid)+" sekundit kulus."

		else:
			return str(int(minutid))+" minutit ja "+str(sekundid)+" sekundit kulus."

	else:
		return
