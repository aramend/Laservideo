import cv2
import gs
import argparse
import numpy as np
from time import *
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser(description="Kaks argumenti, sisendfail ja väljundfail")

parser.add_argument("input", metavar="sisendfaili nimi", type=str)
parser.add_argument("output", metavar="väljundfaili nimi", type=str)

args = parser.parse_args()


start_time = clock()

sisend = cv2.imread(args.input, 0).astype('float64')
gs_pilt = (gs.GerchbergSaxton(sisend, 10))
kontroll = abs(gs.fourier(np.exp(1j*(gs_pilt/193)*(2*np.pi))))

#csv_pilt = gs.csv_calib(gs_pilt)
polyfit_pilt = gs.i_calib(gs_pilt)
cv2.imwrite(args.output, polyfit_pilt)

print(clock() - start_time)

plt.subplot(131), plt.imshow(sisend, cmap = 'gray')
plt.title('Sisend'), plt.xticks([]), plt.yticks([])
plt.subplot(132), plt.imshow(gs_pilt, cmap = 'gray')
plt.title('Gerchberg-Saxton'), plt.xticks([]), plt.yticks([])
plt.subplot(133), plt.imshow(kontroll, cmap = 'gray')
plt.title('Kontroll'), plt.xticks([]), plt.yticks([])
plt.show()