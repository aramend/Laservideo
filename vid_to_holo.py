import cv2
import gs
import numpy as np
import argparse
from time import clock

start_time = clock()

parser = argparse.ArgumentParser(description="Kaks argumenti, sisendfail ja väljundfail")

parser.add_argument("input", metavar="sisendfaili nimi", type=str)
parser.add_argument("output", metavar="väljundfaili nimi", type=str)

args = parser.parse_args()

sisend = cv2.VideoCapture(args.input)
print("Sisend avatud:", sisend.isOpened())

#Sisendi omadused
fps = sisend.get(cv2.CAP_PROP_FPS)
h = int(sisend.get(cv2.CAP_PROP_FRAME_HEIGHT))
w = int(sisend.get(cv2.CAP_PROP_FRAME_WIDTH))
length = sisend.get(cv2.CAP_PROP_FRAME_COUNT)
codec = cv2.VideoWriter_fourcc(*"FMP4") #int(sisend.get(cv2.CAP_PROP_FOURCC))


#Nende abil määrame väljundi
valjund = cv2.VideoWriter(args.output, codec, fps, (1024, 768), False)

c = 1

statistika = [] #Leiame keskmise aja, mis kulub ühe kaadri töötlemiseks

print("\n")



while sisend.isOpened():
	ret, frame = sisend.read()

	if ret==True:

		hallpilt = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		#cv2.imshow("pilt", hallpilt)
		#cv2.waitKey(0)

		kaader = np.full((768, 1024), 0, dtype=hallpilt.dtype)
		kaader[384:,512:] = hallpilt

		#cv2.imshow("kaader", kaader)
		#cv2.waitKey(0)

		algus = clock()
		hologramm = gs.GerchbergSaxton(kaader, 5).astype("u1")
		kalibreeritud = gs.i_calib(hologramm)
		#print(np.min(kalibreeritud), np.max(kalibreeritud))
		statistika.append(clock()-algus)

		valjund.write(kalibreeritud)

		gs.progress(length,c)
		"""
		osakaal = (c/length)*100
		print(str(round(osakaal,2))+"% valmis")
		"""
		c += 1


	else:
		break


sisend.release()
valjund.release()
cv2.destroyAllWindows()

print("\n")

kulunud_aeg = clock()-start_time
if len(statistika) > 0:
	keskmine = sum(statistika)/len(statistika)

	print("\n"+gs.aeg(kulunud_aeg))
	print("\nKeskmine aeg, mis kulus kaadri kodeerimisele:", round(keskmine, 5),"\n")
    